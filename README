################################################################################
######## Sample source code of an Agent-based Simulation:
######## Publication: 
######## "The role of vaccination coverage, individual behaviors, 
######## and the public health response in the control of measles epidemics: 
######## an agent-based simulation for California."
######## F. Liu, W.T. Enanoria, J. Zipprich, BMC Public Health 2015
######## URL:
######## https://www.ncbi.nlm.nih.gov/pubmed/25928152
################################################################################

# decompress file "FRED_measles.tar.gz" and change the current directory to folder "FRED_measles"
$ tar xzvf FRED_measles.tar.gz
$ cd FRED_measles

# change directory to the source code folder "src", remove the old version and re-compile it
$ cd src
$ rm *.o
$ make

# change directory to folder FRED_measles, 
# set up environment variable $FRED_HOME and add the local "bin" to $PATH
$ cd ..
$ cwd="$PWD"; export FRED_HOME="$cwd";export PATH="$FRED_HOME/bin":$PATH

# the folder "work" is where we run the measles simulation
# change directory to "work" and run a test
$ cd work
$ run_fred -p params -n 2 -d OUT
  --------    ------    -    ---
  |           |         |    |name of a folder to save the outputs
  |           |         |
  |           |         |number of iterations 
  |           |         
  |           |name of the parameter files
  |
  |command to run the measles simulation in FRED

# there is a parameter called "days" in parameter file "params", its current value is 90
# now change it to 99:
$ ch 'days' '99' ./params
  --  ----   --  --------
  |   |      |   |path to a parameter file
  |   |      |
  |   |      |new value
  |   |
  |   |name of the parameter
  |
  |command to change the value of a parameter in a parameter file

# details of output files in folder "OUT":
$ cd OUT
OUT
├── COMMAND_LINE          # the command we just ran: run_fred -p params -n 2 -d OUT
├── ContactTracing1.txt   # individual data: who contacted whom on each simulated day in iteraion 1
├── ContactTracing2_1.txt # individual data: contact tracing data on each simulated day in iteraion 1
├── ContactTracing2_2.txt # individual data: contact tracing data on each simulated day in iteraion 2
├── ContactTracing2.txt   # individual data: who contacted whom on each simulated day in iteraion 2
├── ContactTracing3_1.txt # individual data: intervention records on each simulated day in iteration 1
├── ContactTracing3_2.txt # individual data: intervention records on each simulated day in iteration 2
├── ContactTracing4_1.txt # not used
├── ContactTracing4_2.txt # not used
├── infections1.txt       # individual data: who infected whom on each simulated day in iteration 1
├── infections2.txt       # individual data: who infected whom on each simulated day in iteration 2
├── LOG1                  # log file of iteration 1
├── LOG2                  # log file of iteration 2
├── out1.txt              # population data from iteration 1: please see 
                          ## pages 19 and 20 in ./FRED_UserGuide.pdf
├── out2.txt              # population data from iteration 2
├── params                # parameter file used for this simulation.
                          # the measles model has 23 parameters:
                          # 14 calibrated parameters
                          # [1] "V"                     "a"                     "household_contacts"   
                          # [4] "neighborhood_contacts" "workplace_contacts"    "school_contacts"      
                          # [7] "intervention_delay1"   "intervention_delay2"   "self_report_delay"    
                          # [10] "pep_vaccine_efficacy"  "pep_ig_efficacy"       "home_quarantine_prob" 
                          # [13] "home_stay_prob"        "daycare_contacts"     
                          # 9 fixed parameters
                          # [1] "household_prob"     "neighborhood_prob"  "workplace_prob"    
			  # [4] "school_prob"        "vaccine_efficacy"   "trace_prob"        
                          # [7] "cooperation_prob"   "contact_found_prob" "daycare_prob"      
                          # for other default parameters from FRED, 
                          ## please see pages 14 and 15 in ./FRED_UserGuide.pdf.
├── vacctr1.txt           # not used
└── vacctr2.txt           # not used

# we have calibrated 14 parameters by running mcmc with 4 chains:
$ ls chain.records*.csv
chain.records1.csv  chain.records2.csv  chain.records3.csv  chain.records4.csv

# the Gelman-Rubin diagnostic test results indicate the convergence of 7 parameters:
# [1] "V"                     "a"                     "neighborhood_contacts"
# [4] "intervention_delay2"   "pep_vaccine_efficacy"  "home_stay_prob"       
# [7] "daycare_contacts"     
# for details of the convergence test, please see the R code "mcmc.convergence.R"
$ R CMD BATCH mcmc.convergence.R

# the final step is to use the calibrated parameter sets from mcmc chains to run measles simulations
# under 8 combinations of PEP_vaccine,  PEP_ig and Home_quarantine.
# the R code "run.Humboldt.R" selects some parameter sets from "chain.records1.csv" after burn-in;
# for each selected parameter set, it runs 256 iterations for each of 8 intervention scenarios
# and saves outputs to the folder "RESULT".

# in "run.Humboldt.R", to adjust the lengths of burn-in and stride, or use other chains,
# you may modify these lines:

fitted.pars <- read.csv("./chain.records1.csv")[-1]
burnin <- 1000
stride <- 140
step.id <- seq(from=burnin,to=nrow(fitted.pars),by=stride)


# details of files in folder "work":
work
├── acf.7.params.tiff                         # output file from mcmc.convergence.R
├── chain.records1.csv                        # chain1
├── chain.records2.csv                        # chain2
├── chain.records3.csv                        # chain3
├── chain.records4.csv                        # chain4
├── cluster_vacc                              # called by run.Humboldt.R: initialize immunity clustering
├── countyCode.csv                            # CA counties table
├── FRED_UserGuide.pdf                        # FRED user guide
├── gelman.7.params.tiff                      # output file from mcmc.convergence.R
├── gelman.rubin.diagnostic.7.params.tiff     # output file from mcmc.convergence.R
├── gmon.out
├── immue.age.csv                             # age specific immunity data
├── mcmc.convergence.R                        # convergence test code
├── mcmc.convergence.R~
├── mcmc.convergence.Rout
├── OUT                                       # see above
│   ├── COMMAND_LINE
│   ├── ContactTracing1.txt
│   ├── ContactTracing2_1.txt
│   ├── ContactTracing2_2.txt
│   ├── ContactTracing2.txt
│   ├── ContactTracing3_1.txt
│   ├── ContactTracing3_2.txt
│   ├── ContactTracing4_1.txt
│   ├── ContactTracing4_2.txt
│   ├── infections1.txt
│   ├── infections2.txt
│   ├── LOG1
│   ├── LOG2
│   ├── out1.txt
│   ├── out2.txt
│   ├── params
│   ├── vacctr1.txt
│   └── vacctr2.txt
├── params                                  # test parameter files
├── params.000                              # parameter file of scenario 1
├── params.001                              # parameter file of scenario 3
├── params.010                              # parameter file of scenario 4
├── params.011                              # parameter file of scenario 5
├── params.100                              # parameter file of scenario 6
├── params.101                              # parameter file of scenario 7
├── params.110                              # parameter file of scenario 8
├── params.111                              # parameter file of scenario 2
├── pop_by_county                           
│   ├── loc_fred.Humboldt.txt               # synthetic population data 
│   ├── loc_fred.Sacramento.txt             # synthetic population data 
│   ├── pop_fred.Humboldt.txt               # synthetic population data 
│   └── pop_fred.Sacramento.txt             # synthetic population data 
├── pop_fred.Humboldt.csv                   # synthetic population data 
├── pop_fred.Humboldt.original.txt          # synthetic population data 
├── pop_fred.Humboldt.txt                   # synthetic population data with vaccine status
├── pop_fred.Humboldt.vacc.csv              # synthetic population data 
├── primary_case_schedule-0.txt             # number of index case
├── RESULT                                  # output folder of run.Humboldt.R
│   ├── Humboldt                            # folder of Humboldt
│   │   ├── Humboldt1.1        # folder of Humboldt to save outputs of scenario 1 and parameter set #1
│   │   │   ├── DATA
│   │   │   │   └── OUT                     
│   │   │   │       ├── big.table.csv       # summary of outputs
│   │   │   │       ├── COMMAND_LINE
│   │   │   │       ├── out1.txt
│   │   │   │       ├── out2.txt
│   │   │   │       ├── params.000          
│   │   │   │       ├── vacctr1.txt
│   │   │   │       └── vacctr2.txt
│   │   │   └── params.000
│   │   ├── Humboldt1.2        # folder of Humboldt to save outputs of scenario 2 and parameter set #1
│   │   │   ├── DATA
│   │   │   │   └── OUT
│   │   │   │       ├── big.table.csv
│   │   │   │       ├── COMMAND_LINE
│   │   │   │       ├── out1.txt
│   │   │   │       ├── out2.txt
│   │   │   │       ├── params.111
│   │   │   │       ├── vacctr1.txt
│   │   │   │       └── vacctr2.txt
│   │   │   └── params.111
│   │   ├── Humboldt1.3        # folder of Humboldt to save outputs of scenario 3 and parameter set #1
│   │   │   ├── DATA
│   │   │   │   └── OUT
│   │   │   │       ├── big.table.csv
│   │   │   │       ├── COMMAND_LINE
│   │   │   │       ├── out1.txt
│   │   │   │       ├── out2.txt
│   │   │   │       ├── params.001
│   │   │   │       ├── vacctr1.txt
│   │   │   │       └── vacctr2.txt
│   │   │   └── params.001
│   │   ├── Humboldt1.4        # folder of Humboldt to save outputs of scenario 4 and parameter set #1
│   │   │   ├── DATA
│   │   │   │   └── OUT
│   │   │   │       ├── big.table.csv
│   │   │   │       ├── COMMAND_LINE
│   │   │   │       ├── out1.txt
│   │   │   │       ├── out2.txt
│   │   │   │       ├── params.010
│   │   │   │       ├── vacctr1.txt
│   │   │   │       └── vacctr2.txt
│   │   │   └── params.010
│   │   ├── Humboldt1.5        # folder of Humboldt to save outputs of scenario 5 and parameter set #1
│   │   │   ├── DATA
│   │   │   │   └── OUT
│   │   │   │       ├── big.table.csv
│   │   │   │       ├── COMMAND_LINE
│   │   │   │       ├── out1.txt
│   │   │   │       ├── out2.txt
│   │   │   │       ├── params.011
│   │   │   │       ├── vacctr1.txt
│   │   │   │       └── vacctr2.txt
│   │   │   └── params.011
│   │   ├── Humboldt1.6        # folder of Humboldt to save outputs of scenario 6 and parameter set #1
│   │   │   ├── DATA
│   │   │   │   └── OUT
│   │   │   │       ├── big.table.csv
│   │   │   │       ├── COMMAND_LINE
│   │   │   │       ├── out1.txt
│   │   │   │       ├── out2.txt
│   │   │   │       ├── params.100
│   │   │   │       ├── vacctr1.txt
│   │   │   │       └── vacctr2.txt
│   │   │   └── params.100
│   │   ├── Humboldt1.7        # folder of Humboldt to save outputs of scenario 7 and parameter set #1
│   │   │   ├── DATA                      
│   │   │   │   └── OUT
│   │   │   │       ├── big.table.csv
│   │   │   │       ├── COMMAND_LINE
│   │   │   │       ├── out1.txt
│   │   │   │       ├── out2.txt
│   │   │   │       ├── params.101
│   │   │   │       ├── vacctr1.txt
│   │   │   │       └── vacctr2.txt
│   │   │   └── params.101
│   │   ├── Humboldt1.8       # folder of Humboldt to save outputs of scenario 8 and parameter set #1
│   │   │   ├── DATA
│   │   │   │   └── OUT
│   │   │   │       ├── big.table.csv
│   │   │   │       ├── COMMAND_LINE
│   │   │   │       ├── out1.txt
│   │   │   │       ├── out2.txt
│   │   │   │       ├── params.110
│   │   │   │       ├── vacctr1.txt
│   │   │   │       └── vacctr2.txt
│   │   │   └── params.110
│   │   ├── Humboldt7788.1    # folder of Humboldt to save outputs of scenario 1 and parameter set #7788
│   │   │   ├── DATA
│   │   │   │   └── OUT
│   │   │   │       ├── COMMAND_LINE
│   │   │   │       ├── ContactTracing1.txt
│   │   │   │       ├── ContactTracing2_1.txt
│   │   │   │       ├── ContactTracing2_2.txt
│   │   │   │       ├── ContactTracing2.txt
│   │   │   │       ├── ContactTracing3_1.txt
│   │   │   │       ├── ContactTracing3_2.txt
│   │   │   │       ├── ContactTracing4_1.txt
│   │   │   │       ├── ContactTracing4_2.txt
│   │   │   │       ├── infections1.txt
│   │   │   │       ├── infections2.txt
│   │   │   │       ├── LOG1
│   │   │   │       ├── LOG2
│   │   │   │       ├── out1.txt
│   │   │   │       ├── out2.txt
│   │   │   │       ├── params.000
│   │   │   │       ├── vacctr1.txt
│   │   │   │       └── vacctr2.txt
│   │   │   └── params.000
│   │   └── Humboldt7788.2   # folder of Humboldt to save outputs of scenario 1 and parameter set #7788
│   │       ├── DATA
│   │       │   └── OUT
│   │       │       ├── big.table.csv
│   │       │       ├── COMMAND_LINE
│   │       │       ├── out1.txt
│   │       │       ├── out2.txt
│   │       │       ├── params.111
│   │       │       ├── vacctr1.txt
│   │       │       └── vacctr2.txt
│   │       └── params.111
│   └── simout.csv
├── run.Humboldt.R
├── run.Humboldt.R~
├── test.run.sh
└── vacc_status.csv         # regenerated vaccine status of each individual before simulation
